/*
  ZenDrive.h

  Copyright (c) 2021, Valentin Boulanger

  This library is a part of the ZenDrive simulator firmware.
  Version : 1.3.2
*/

#ifndef ZENDRIVE_h
#define ZENDRIVE_h

#include "HID.h"

#if ARDUINO < 10606
#error The ZenDrive library requires Arduino IDE 1.6.6 or greater. Please update your IDE.
#endif

#if ARDUINO > 10606
#if !defined(USBCON)
#error The ZenDrive library can only be used with a USB MCU (e.g. Arduino Leonardo, Arduino Micro, etc.).
#endif
#endif

#if !defined(_USING_HID)

#warning "Using legacy HID core (non pluggable)"

#else

//================================================================================
//================================================================================
//  ZenDrive Simulator (Game Controller)

class ZenDrive_
{
private:

  // Gearshift module
  uint8_t  speeds;                    // (NEUTRAL;SPEED1;SPEED2;SPEED3;SPEED4;SPEED5;SPEED6;SPEEDR)
	uint8_t	 handbrake;

  //Pedals module
	uint8_t	 clutch;
	uint8_t	 brake;
	uint8_t	 accelerator;

  //Steering wheel module
	uint16_t	 wheelCommands;             // (BLINKER LEFT;BLINKER RIGHT;WARNING;LIGHTS;HEAD LIGHTS;FOGLIGHTS;STARTER;HORN;CRUISE;CRUISE +;CRUISE -;NOT USED;NOT USED;NOT USED;NOT USED;NOT USED)
  int8_t   steering;

public:
	ZenDrive_();

	void begin();
	void end();

  // Gearshift module
  bool gearshiftNeedRefresh = false;
  int currentSpeed = 0;
  void switchNeutral();
  void switchSpeed1();
  void switchSpeed2();
  void switchSpeed3();
  void switchSpeed4();
  void switchSpeed5();
  void switchSpeed6();
  void switchSpeedR();
	void setHandbrake(int8_t value);

  // Pedals module
  bool pedalsNeedRefresh = false;
	void setClutch(int8_t value);
	void setBrake(int8_t value);
	void setAccelerator(int8_t value);

  // Steering wheel module
  bool wheelNeedRefresh = false;
  bool isBlinkerLeft = false;
  void setBlinkerLeft(bool value);
  bool isBlinkerRight = false;
  void setBlinkerRight(bool value);
  bool isWarning = false;
  void setWarning(bool value);
  bool isLights = false;
  bool isRoadLights = false;
  void switchLightsOn();
  void switchRoadLights();
  void switchLightsOff();
  bool isHeadLights = false;
  void setHeadLights(bool value);
  bool isFogLights = false;
  void setFogLights(bool value);
  bool isStarter = false;
  void setStarter(bool value);
  bool isHorn = false;
  void setHorn(bool value);
  void activeCruise();
  void increaseCruise();
  void decreaseCruise();
	void setDirection(uint8_t value);

	void sendGearshiftStates();
  void sendPedalsStates();
  void sendWheelStates();
};
extern ZenDrive_ ZenDrive;

#endif
#endif
